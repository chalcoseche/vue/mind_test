### nuxt 명령어
* 새로운 프로젝트 생성 : npx nuxi init 프로젝트 이름.  
* nuxt.config.ts 수정   
```
export default defineNuxtConfig( {
    css: [
        '@/assets/'layout.css',
        '@/assets/'contents.css',
    ],
    components: [
        {
            path: '~/components',
            extensions:['.vue'],
        }
    ]
})
```

* config 에 맞춰 assets 폴더와 components 폴더 생성 

* ```
// app.vue에  생성
export default {
  data(){
    return {
        resultList: [

        ]
    }
  }
}
```
* **여러개 작성시에는 배열사용할것**  
(json 스타일 만들어볼것)




>### 0218

**리액트랑 다른점**

*if문을 div 태그 안에 넣는다.
`<div v-if=""></div>`  
그다음 조건문은 else if 해준다  
`<div v-else-if=""></div>  
*onEvent 대신 @click @change 같이 사용함  

* <template> 으로 감싸준다.  
* class 컴포넌트와 비슷한거같은데 ( this 사용)
data() 에는 return값을, 
그밑에는 method 를 입력하여 함수작성한다 ) 함수가 밑으로 
}  




>### js 기반 패키지 사용 해보기 (npm)

* vue gallery 검색  

> #### element UI 사용해보기



* element ui 는 vue3 전용  
* 사용중인건 nuxt3.js에서 element UI 사용하는법   
[링크] [링크]:https://greeng00se.tistory.com/73  
* `npm i --save-dev @types/node`
* 다음 의존성 : 추가패키지   
(의존성 : devDependencies)

`sass  
element-plus  
@element-plus/icons-vue  
unplugin-vue-components  
unplugin-auto-import 
`

*nuxt 설정 하기  

```
//nuxt.config.ts 에 병합하기 
import {defineNuxtConfig} from "nuxt/config";

const lifecycle = process.env.npm_lifecycle_event;

export default defineNuxtConfig({
css:["~/assets/scss/index.scss"],
build:{
    transpile: lifecycle ==="build"? ["element-plus"]:[],
},
});

//assets/scss/index.scss 생성
@use "element-plus/dist/index.css";
//root 폴더의 pages 폴더 생성  
//pages/index.vue 만들기
```

* https://element-plus.org/en-US/ 사용하기
* element plus