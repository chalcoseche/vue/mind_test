// https://nuxt.com/docs/api/configuration/nuxt-config
import {defineNuxtConfig} from "nuxt/config";

const lifecycle = process.env.npm_lifecycle_event;

export default defineNuxtConfig({
modules: ['@element-plus/nuxt'],
css :[
    '~/assets/layout.css',
    '~/assets/contents.css',
    "~/assets/scss/index.scss"
],
build:{
    transpile: lifecycle ==="build"? ["element-plus"]:[],
},
components: [
    {
        path:'~/components',
        extensions:['.vue'],
    }
],
})
